# Benchmark GeoPackage vs Shapefile, revisión en 2021

El GPKG es un formato relativamente reciente pero ya consolidado como el estándar vectorial casi-todo-propósito moderno, desde su incorporación como formato default en QGIS > 3.0, en reemplazo del shapefile (que sirve menos propósitos que el GPKG). Como un formato abierto está en constante desarrollo, y no solo este, sino SQLite que es el motor que le da vida, por lo cual quise _recrear y ampliar_ un benchmark disponible en __switchfromshapefile.org/compare.html 2017__, para ver cómo ha avanzado SQLite frente al SHP.

Además de la ventaja de ser _un solo archivo_ y poder tener nombres tan largos como se quiera en las columnas, entre otras, el GPKG es veloz, lo cual veremos en las siguientes líneas.

El archivo de 3.5 GB descomprimido es descargable desde: http://download.geofabrik.de/europe/netherlands.html

## Pre-proceso

__*Este paso no fue hecho en el anterior benchmark*__ 

El primer paso es convertir el Shapefile a GPKG, con un simple `ogr2ogr -f GPKG gis.osm_buildings_a_free_1.gpkg gis.osm_buildings_a_free_1.shp`

Para que estén en igualdad de condiciones creamos un índice espacial para el Shapefile (archivo .qix), puesto que se creó un índice (por default) en el primer paso al convertir a GPKG, podemos confirmarlo en la última línea como `HasSpatialIndex (Integer) = 1`:

```sh
ogrinfo -sql "SELECT HasSpatialIndex('gis.osm_buildings_a_free_1', 'geom')" gis.osm_buildings_a_free_1.gpkg
# en el extraño caso que no lo tenga, se crea con 
ogrinfo -sql "SELECT createspatialindex('gis.osm_buildings_a_free', 'geom')" gis.osm_buildings_a_free_1.gpkg
# las comillas sencillas son primordiales
...
...
 HasSpatialIndex (Integer) = 1
```

Para crear el índice para el Shapefile:

```sh
ogrinfo -sql "CREATE SPATIAL INDEX ON gis.osm_buildings_a_free_1" gis.osm_buildings_a_free_1.shp
```

Entonces ahora replicamos el benchmark. __Importante:__ este nuevo benchmark se hizo en una computadora con disco mecánico mientras que el anterior fue en uno de estado sólido.

## TAMAÑO DE ARCHIVO

Formato | Tamaño
--- | ---:
shp | 3.5 GB (con 56 MB de índice)
gpkg | 2.8 GB

El tamaño de archivo prácticamente no cambió desde el primer benchmark de 2017.

## PRIMERA CONSULTA

Solamente consultamos los metadatos

```sh
ogrinfo -so gis.osm_buildings_a_free_1.shp gis.osm_buildings_a_free_1
```

_Los valores de `time` son "real" que es el tiempo de ejecución tipo "wall clock time"_

Formato | tiempo actual | tiempo anterior
--- | --- | ---:
shp | 0m0.356s | ~ 0.4s
gpkg |	0m0.149s | ~ 3.1 s

Antes las consultas pequeñas eran onerosas para el GPKG, ahora es dos veces más rápido que el Shapefile.

## SEGUNDA CONSULTA

Solo un simple filtro de feature ID:

```sh
time ogrinfo -fid 63347 gis.osm_buildings_a_free_1.shp gis.osm_buildings_a_free_1
```

Formato | tiempo actual | tiempo anterior
--- | --- | ---:
shp | 0m0.366s | ~0.4s
gpkg |	0m0.151s | ~2.9s

La misma observación que para la primera consulta.

## TERCERA CONSULTA

Filtrar por un bounding box (encuadre).

```sh
time ogrinfo -spat 5.421254 52.129629 5.421254 52.129629 gis.osm_buildings_a_free_1.shp gis.osm_buildings_a_free_1
```

Formato | tiempo actual | tiempo anterior
--- | --- | ---:
shp | 0m0.398s | ~ 17s
gpkg | 0m0.154s | ~0.05 s

La abrupta mejora para el Shapefile es porque en este benchmark creamos un **índice espacial**

## CUARTA CONSULTA

Un filtro no espacial

```sh
time ogrinfo -where "name='OBS De Hobbit'" gis.osm_buildings_a_free_1.shp gis.osm_buildings_a_free_1
```

Formato | tiempo actual | tiempo anterior
--- | --- | ---:
shp |	1m17.867s | ~41s
gpkg |	0m6.085s | ~8.5s

Naturalmente la ventaja en consultas SQL es evidente para un formato construído sobre SQLite

## QUINTA CONSULTA: empieza más geoespacial

Aquí empezamos con consultas que no fueron hechas en el primer test; arrancamos con una simple re-proyección y cálculo de área de el primer feature:

```sh
ogrinfo -dialect sqlite -sql 'select st_area(st_transform(geometry,3857)), osm_id, name from  "gis.osm_buildings_a_free_1" limit 1' gis.osm_buildings_a_free_1.shp
```

Formato | tiempo actual 
--- | ---:
shp |  0m0.836s
gpkg |  0m0.312s

Gana GPKG

## SEXTA CONSULTA

El área del buffer del centroide reproyectado del primer polígono.

```sh
time ogrinfo -dialect sqlite -sql 'select st_area(st_buffer(st_transform(st_centroid(geometry),3857),100)) , osm_id, name from  "gis.osm_buildings_a_free_1" limit 1' gis.osm_buildings_a_free_1.shp
```

Formato | tiempo actual 
--- | ---:
shp  | 0m0.803s
gpkg  | 0m0.187s

Pierde Shapefile

## SÉPTIMA CONSULTA

Solo buscamos el área más grande de todos los polígonos (convendría reproyectar pero para el Shapefile toma mucho tiempo)

```sh
time ogrinfo -dialect sqlite -sql 'select max(st_area(geom)), osm_id, name from  "gis.osm_buildings_a_free_1"' gis.osm_buildings_a_free_1.gpkg
```
Formato | tiempo actual 
--- | ---:
shp | 5m10.623s
gpkg |	0m58.236s

5 veces más lento el Shapefile.


# Conclusiones:

Conviene usar el formato GPKG (si valoras tu tiempo).

